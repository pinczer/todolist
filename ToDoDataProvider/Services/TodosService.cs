﻿using System.Collections.Generic;
using TodoDomain;
using TodoDomain.Repositories;

namespace TodoDataProvider.Services
{
    public class TodosService : ITodosService
    {
        private ITodosRepository _repository;

        public TodosService(ITodosRepository repository)
        {
            _repository = repository;
        }

        public Todo AddTodo(Todo item)
        {
            return _repository.Add(item);
        }

        public Todo UpdateTodo(int id, Todo item)
        {
            item.Id = id;
            return _repository.Update(item);
        }

        public void DeleteTodo(int id)
        {
            var item = _repository.Get(id);
            if (item == null)
            {
                return;
            }
            _repository.Delete(item);
        }

        public Todo MarkAsDone(int id)
        {
            var item = _repository.Get(id);
            if (item == null)
            {
                return item;
            }
            item.IsDone = true;

            return UpdateTodo(item.Id, item);
        }

        public IEnumerable<Todo> GetAll()
        {
            return _repository.GetAll();
        }

        public Todo Get(int id)
        {
            return _repository.Get(id);
        }
    }
}
