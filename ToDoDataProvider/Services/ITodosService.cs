﻿using System.Collections.Generic;
using TodoDomain;

namespace TodoDataProvider.Services
{
    public interface ITodosService
    {
        Todo AddTodo(Todo item);
        void DeleteTodo(int id);
        IEnumerable<Todo> GetAll();
        Todo MarkAsDone(int id);
        Todo UpdateTodo(int id, Todo item);
        Todo Get(int id);
    }
}