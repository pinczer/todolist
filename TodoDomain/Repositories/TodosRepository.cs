using System.Collections.Generic;
using System.Linq;

namespace TodoDomain.Repositories
{
    public class TodosRepository : ITodosRepository
    {
        private TodoContext _context;

        public TodosRepository(TodoContext context)
        {
            _context = context;
        }

        public Todo Add(Todo item)
        {
            item = _context.Todos.Add(item).Entity;
            _context.SaveChanges();
            return item;
        }

        public Todo Update(Todo item)
        {
            item = _context.Todos.Update(item).Entity;
            _context.SaveChanges();
            return item;
        }

        public void Delete(Todo item)
        {
            _context.Todos.Remove(item);
            _context.SaveChanges();
        }

        public Todo Get(int id)
        {
            return _context.Todos.FirstOrDefault(t => t.Id == id);
        }

        public IEnumerable<Todo> GetAll()
        {
            return _context.Todos.ToList();
        }
    }
}
