﻿using System.Collections.Generic;

namespace TodoDomain.Repositories
{
    public interface ITodosRepository
    {
        Todo Add(Todo item);
        void Delete(Todo item);
        Todo Get(int id);
        IEnumerable<Todo> GetAll();
        Todo Update(Todo item);
    }
}