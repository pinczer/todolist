namespace TodoDomain
{
    public class Todo
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsDone { get; set; }
    }
}
