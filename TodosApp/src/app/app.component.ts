import { Component, OnInit } from '@angular/core';
import { TodosService } from './todos.service';
import { Todo } from './todo';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [TodosService]
})
export class AppComponent implements OnInit {

    todos: Todo[] = [];
    newTodo: Todo = new Todo();
    orgTodo: Todo;

    constructor(
        private todosService: TodosService
    ) {

    }

    public ngOnInit() {
        this.todosService
            .getAllTodos()
            .subscribe(
            (todos) => {
                this.todos = todos;
            }
            );
    }

    addTodo() {
        this.todosService
            .addTodo(this.newTodo)
            .subscribe(
            (addedTodo) => {
                if (addedTodo === null) {
                    return;
                }
                this.todos = this.todos.concat(addedTodo);
                this.newTodo = new Todo();
            }
        );

    }

    toggleTodoIsDone(todo) {
        todo.isDone = !todo.isDone;
        this.todosService
            .updateTodo(todo)
            .subscribe(
            (updatedTodo) => {
                todo = updatedTodo;
                this.todos.filter((t) => t.id === todo.id)[0].isDone = todo.isDone;
            }
            );
    }

    editTodo(todo) {
        this.cancelEdit();
        this.orgTodo = new Todo(todo);
        todo.isUpdating = true;
    }

    cancelEdit() {
        var editedTodoIndex = this.todos.findIndex((t) => t.isUpdating === true);
        if (editedTodoIndex >= 0) {
            this.todos[editedTodoIndex] = this.orgTodo;
        }
    }

    updateTodo(todo) {
        this.todosService
            .updateTodo(todo)
            .subscribe(
            (updatedTodo) => {
                if (updatedTodo == null) {
                    this.cancelEdit();
                    return;
                }
                todo = updatedTodo;
                this.todos.filter((t) => t.id === todo.id)[0].isUpdating = false;
            }
        );
        
    }

    removeTodo(todo) {
        this.todosService
            .deleteTodoById(todo.id)
            .subscribe(
            (_) => {
                this.todos = this.todos.filter((t) => t.id !== todo.id);
            }
            );
    }
}