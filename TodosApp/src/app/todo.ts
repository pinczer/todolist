export class Todo {

    id: number;
    content: string = '';
    isDone: boolean = false;
    isUpdating: boolean = false;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}
