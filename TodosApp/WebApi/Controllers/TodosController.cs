﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TodoDomain;
using TodoDataProvider.Services;

namespace TodosApp.Controllers
{
    [Produces("application/json")]
    [Route("api/todos")]
    public class TodosController : Controller
    {
        private readonly ITodosService _service;

        public TodosController(ITodosService service)
        {
            _service = service;
        }

        // GET: api/Tasks
        [HttpGet]
        public IEnumerable<Todo> Get()
        {
            return _service.GetAll();
        }

        // POST: api/Tasks
        [HttpPost]
        public IActionResult Add([FromBody]Todo item)
        {
            if (string.IsNullOrEmpty(item.Content))
            {
                return NoContent();
            }
            return Ok(_service.AddTodo(item));
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Todo item)
        {
            if (string.IsNullOrEmpty(item.Content))
            {
                return NoContent();
            }

            return Ok(_service.UpdateTodo(id, item));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.DeleteTodo(id);
        }
    }
}